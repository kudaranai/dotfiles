#########################
##  AUTHOR: kudaranai  ##
#########################

#####################
##  LOAD STARSHIP  ##
#####################

starship init fish | source

###################
##  FISH CONFIG  ##
###################

## disable fish greeting
set fish_greeting

###############
##  ALIASES  ##
###############

## terminal aliases

alias nf="neofetch | lolcat"
alias rf="rm -rf"
alias e="exit"
alias :q="exit"
alias h="history"
alias la="ls -lah"
alias ll="ls -lah"
alias l="ls -lah"

## config aliases

alias fc="vim $HOME/.config/fish/config.fish"
alias nc="vim $HOME/.config/neofetch/config.conf"
alias pc="vim $HOME/.config/polybar/config"
alias i3c="vim $HOME/.config/i3/config"

## pacman aliases

alias syu="sudo pacman -Syu"
alias syy="sudo pacman -Syy"
alias rns="sudo pacman -Rns"
alias s="sudo pacman -S"
alias R="sudo pacman -R"

## yay aliases

alias ysyu="yay -Syu -a"
alias ysyy="yay -Syy"
alias yrns="yay -Rns"
alias ys="yay -S"
alias yr="yay -R"

## git aliases

alias ga="git add -A"
alias gc="git commit -m"
alias push="git push"
alias gpm="git push origin master"
alias gl="git log"
alias gs="git status"
alias gd="git diff"
alias gb="git branch"
alias gch="git checkout"
alias gra="git remote add"
alias grr="git remote rm"
alias gpull="git pull"
alias gcl="git clone"
alias gt="git tag -a -m"
alias gf="git reflog"
alias git_username="git config --global user.username"
alias git_name="git config --global user.name"
alias git_email="git config --global user.email"
alias gr="git reset --hard"

#############
##  PATHS  ##
#############

export PATH="$HOME/scripts:$PATH"

########################
##  EXECUTE AT START  ##
########################

neofetch | lolcat

