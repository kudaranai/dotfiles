# Dotfiles

These are my dotfiles, based on the KDE Sweet theme.
These dotfiles are meant to work inside a KDE desktop install, but the KDE window manager (kwin) is replaced with i3-gaps.

# How to install

I based my dotfiles around arch based distros so the package names may vary on other non arch based distros.
First of all you need to have an arch based distro running a kde desktop.

1. Clone the repo:

```git clone https://gitlab.com/kudaranai/dotfiles.git```

2. cd into the dotfiles directory and run the following command to install the dependencies. (you need to be on an arch based distro with yay installed).

```yay -S --needed - < packages_to_install.txt```

This command will install the needed packages from pacman if they are available and the ones that are not in pacman will be installed from the AUR.

Also install starship to completely configure the fish shell (skip this if you are going to use ohmyzsh)

```curl -fsSL https://starship.rs/install.sh | bash```

You can also use the starship theme on zsh if you add this line to the bottom of .zshrc:

```starship init zsh | source```

3. Enable hidden files in your file browser of choice and copy everything to the root of your home folder (/home/your_username).

4. Plasma config 
Plasma might need some more tweaking, install the theme sweet from the global theme category in the settings app and change the following things:
- Plasma style: Sweet
- Application style: kvantum-dark (we'll get to this part in a bit)
- Colours: Sweet
- Cursors: Sweet-Cursors

5. Kvantum config
    - 5.1 Open the kvantum manager 
    - 5.2 Select your kvantum theme folder (~/.config/Kvantum/Sweet) and click install theme.
    - 5.3 Go to change/delete theme and select the Sweet theme and click use this theme.
    - 5.4 If it's not configured by default, go to configure active theme and then compositing & general look and set  "reduce window opacity by" to 0.



6. Fonts

Fonts are located in .local/share/fonts
Just to confirm that they installed correctly, move the fonts to their respective folders:

First of all cd into the root directory of the dotfiles folder, then do the commands below.

Nerfont symbols:

```sudo cp .local/share/fonts/'Symbols-1000-em Nerd Font Complete.ttf' /usr/share/fonts/TTF```

Blogger sans:

```sudo mkdir /usr/local/share/fonts/b && sudo cp .local/share/fonts/Blogger_* /usr/local/share/fonts/b```

MesloLGS:

```sudo mkdir /usr/local/share/fonts/m && sudo cp .local/share/fonts/MesloLGS_* /usr/local/share/fonts/m```

7. Screenshots

Normal layout (1 window only)

![Screenshot](https://gitlab.com/kudaranai/dotfiles/-/raw/master/.screenshots/window.png)

Normal layout (2 windows)

![Screenshot](https://gitlab.com/kudaranai/dotfiles/-/raw/master/.screenshots/2_windows.png)

Normal layout (floating)

![Screenshot](https://gitlab.com/kudaranai/dotfiles/-/raw/master/.screenshots/floating_window.png)

Tabbed layout

![Screenshot](https://gitlab.com/kudaranai/dotfiles/-/raw/master/.screenshots/tabbed_windows.png)

Rofi (program launcher)

![Screenshot](https://gitlab.com/kudaranai/dotfiles/-/raw/master/.screenshots/rofi.png)

Dunst (notifications)

![Screenshot](https://gitlab.com/kudaranai/dotfiles/-/raw/master/.screenshots/dunst.png)


