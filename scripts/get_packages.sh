### Script that outputs the current list of installed packages by the user on arch based distros.
### By yumeki.

# Get current date.
CurrentDate=`date +"%Y-%m-%d_%T"`

# Create output folder & cd into it.
mkdir ~/scripts/output/packages/packages-$CurrentDate 
cd ~/scripts/output/packages/packages-$CurrentDate

# Grab list of installed packages.

## Pacman
pacman -Qqen > pacman_packages_$CurrentDate.txt

## AUR
pacman -Qqem > aur_packages_$CurrentDate.txt

## All packages
pacman -Qqe > all_packages_$CurrentDate.txt

exit
